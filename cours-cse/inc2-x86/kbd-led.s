// envoyer la commande 0xed (via l'input buffer)
// pour allumer les LED (qui doivent être spécifiées via
// l'input buffer)

    outb $0xed, $0x60

// attendre que l'input buffer soit vide (donnée d'une
// précédente commande)

boucle:
    inb  $0x64, %al   // AL = status register
    testb $0x02, %al  // teste le bit$_1$ (INPB) du registre AL
    jnz boucle	      // bit$_1 \neq 0 \Rightarrow$ input buffer non vide

// spécifier les LED à allumer via les bits dans l'input buffer
// 0x01 : ScrollLock, 0x02 : NumLock, 0x04 : CapsLock

    outb $0x06, $0x60 // écrire dans l'input buffer
