struct in6_addr {
  // 16 octets dans une union
};

struct sockaddr_in6 {
  uint16_t        sin6_family;    // \texttt{AF\_INET6}
  uint16_t        sin6_port;      // Port
  uint32_t        sin6_flowinfo;  // Flux
  struct in6_addr sin6_addr;      // Adresse IPv6
  uint32_t        sin6_scope_id;
};
