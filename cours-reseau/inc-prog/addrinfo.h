struct addrinfo
{
  int ai_flags;             // pour le paramètre \texttt{indic}
  int ai_family;            // famille d'adresse pour \texttt{socket}
  int ai_socktype;          // \texttt{SOCK\_STREAM} ou \texttt{SOCK\_DGRAM}
  int ai_protocol;          // protocole trouvé
  socklen_t ai_addrlen;     // taille de l'adresse trouvée
  struct sockaddr *ai_addr; // adresse trouvée
  char *ai_canonname;       // nom canonique trouvé
  struct addrinfo *ai_next; // suivant dans la liste
};
