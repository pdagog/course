main ()
{
  int pid ;

  pid = fork () ;
  switch (pid)
  {
    case -1 :         // erreur de fork
      exit (1) ;
    case 0 :          // le démon proprement dit
      setsid () ;     // session propre au démon
      chdir ("/") ;   // répertoire courant
      umask (0) ;
      close (0) ; close (1) ; close (2) ;
      openlog ("exemple", LOG_PID, LOG_DAEMON) ;
      /* ... */
      syslog (LOG_WARN, "attention (%f)", 3.14) ;
      /* ... */
    default :         // terminaison du père
      exit (0) ;
  }
}
