pid_t pid, ppid ;
uid_t uid ;
gid_t gid ;

pid = getpid () ;
printf ("je suis le processus %jd\n", (intmax_t) pid) ;
ppid = getppid () ;
printf ("mon pere est %jd\n", (intmax_t) ppid) ;
uid = getuid () ;
printf ("mon proprio est %ju\n", (intmax_t) uid) ;
gid = getgid () ;
printf ("et mon groupe est %ju\n", (intmax_t) gid) ;
