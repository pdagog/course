# Devine le contenu de ces deux fichiers...
> file ch2-file.tex ch2-file.pdf
ch2-file.tex: LaTeX 2e document, Unicode text, UTF-8 text
ch2-file.pdf: PDF document, version 1.5

# Les 10 premiers octets du fichier \texttt{ch2-file.pdf} en hexadécimal (et en caractère)
> od -t xCc -N 10 ch2-file.pdf
0000000  25  50  44  46  2d  31  2e  35  0a  25
          %   P   D   F   -   1   .   5  \n   %
