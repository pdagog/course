sigset_t e ;		  // déclarer e comme ensemble de signaux
sigemptyset (&e) ;	  // $e \leftarrow \emptyset$ 
sigaddset (&e, SIGINT) ;  // $e \leftarrow e \cup SIGINT$
sigaddset (&e, SIGHUP) ;  // $e \leftarrow e \cup SIGHUP$ \implique e = \{SIGINT, SIGHUP\}
