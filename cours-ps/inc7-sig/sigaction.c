void f (int sig) {	 // fonction appelée lorsque SIGINT est reçu
    ...
}
int main (...) {
  struct sigaction s ;

  s.sa_handler = f ;		 // adresse de la fonction à appeler
  s.sa_flags = 0 ;		 // utilisation « classique »
  sigemptyset (&s.sa_mask) ;	 // cf. plus loin
  sigaction (SIGINT, &s, NULL) ; // définition de l'action
  ...
  // calcul (qui peut être temporairement interrompu par \texttt{f})
}
