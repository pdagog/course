> lspci -tv
-[0000:00]-+-00.0  Intel Corp Pentium Processor Host Bridge/DRAM Registers
           +-02.0  Intel Corp UHD Graphics
           +-04.0  Intel Corp Pentium Core Processor Thermal Subsystem
           +-14.0  Intel Corp Sunrise Point-LP USB 3.0 xHCI Controller
	   ...
           +-1c.2-[02]----00.0  Qualcomm Atheros Wireless Network Adapter
           +-1c.4-[03-6d]----00.0-[04-6d]--+-00.0-[05]--
           |                               +-02.0-[39]----00.0  USB Controller
           |                               \-04.0-[3a-6d]--
	   ...
           +-1f.3  Intel Corp Sunrise Point-LP HD Audio
           \-1f.4  Intel Corp Sunrise Point-LP SMBus

> lsusb -tv
/:  Bus 04.Port 1: Dev 1, Class=root_hub, Driver=xhci_hcd/2p, 10000M
    ID 1d6b:0003 Linux Foundation 3.0 root hub
    |__ Port 1: Dev 2, If 0, Class=Hub, Driver=hub/4p, 5000M
        ID 2109:0817 VIA Labs, Inc. 
        |__ Port 2: Dev 3, If 0, Class=Hub, Driver=hub/4p, 5000M
            ID 2109:0817 VIA Labs, Inc. 
        |__ Port 4: Dev 4, If 0, Class=Vendor Specific Class, Driver=r8152, 5000M
            ID 0bda:8153 Realtek Semiconductor Corp. RTL8153 10 Mb/s Adapter
...
