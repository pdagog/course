#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/ioctl.h>

int main (int argc, char *argv [])
{
// NON INDENTÉ POUR GAGNER DE LA PLACE SUR LA DIAPO
struct termios t1, t2 ; char c ;

ioctl (0, TCGETS, &t1) ;	// lire la configuration du pilote
t2 = t1 ;			// sauvegarder pour restauration à la fin
t2.c_lflag &= ~(ICANON|ECHO) ;	// ne pas faire le pré-traitement et l'écho
ioctl (0, TCSETS, &t2) ;	// enregistrer la nouvelle configuration
for (int i = 0 ; i < 4 ; i++) {	// on se limite à 4 caractères pour cet exemple
    read (0, &c, 1) ;		// lire un caractère...
    printf ("Lu '%c'\n", c) ;	// ... et l'afficher aussitôt
}
ioctl (0, TCSETS, &t1) ;	// restaurer la configuration initiale
}
