> cc -c exemple.c               # génère exemple.o
> nm exemple.o
0000000000000000 T main		# T : symbole défini dans la zone "text"
                 U printf	# U : symbole utilisé, mais non défini ici
0000000000000004 C x		# C : "commun" (peut être défini plusieurs fois)

> cc -static exemple.o		# génère a.out (et attribue les adresses)
> nm a.out | egrep ' (main|printf|x)$'
0000000000401ce5 T main         # l'adresse est maintenant attribuée
0000000000410ba0 T printf       # idem pour printf, fonction incluse dans le fichier
00000000004c3300 B x		# symbole placé dans le BSS (segment "data") $\Rightarrow$ zone initialisée à 0
